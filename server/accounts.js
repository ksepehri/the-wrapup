Accounts.onCreateUser(function(options, user) {
	if(!checkUsername(user.username)) {
		Winston.error('onCreateUser',{user: user, err: 'Restricted username'});
		throw new Meteor.Error(500,'Username is not available','not available');
	}
	if(user.username)
		user.username = user.username.toLowerCase();
	if(options.profile)
		user.profile = options.profile;

  	return user;
});

Meteor.methods({
	searchUsername: function(username) {
	    this.unblock();

	    username = username.toLowerCase();
	    // make sure username is > 3 chars and not reserved
	    // console.dir(username);
	    if(username.length < 3 || !checkUsername(username))
	    	return false;

	    var result = Meteor.users.findOne({username: username});
	    
	    //if you get a return that means the username is taken
	    if(result)
	      return false;
	    else
	      return true;
	  },
	createAccount: function(options) {
		if(!isValidUsername(options.username))
			return {error: 'Username must be at least 3 characters long'};
		if(!isValidPassword(options.password))
			return {error: 'Password must be at least 6 characters long'};

		var userId = Accounts.createUser(options);
		// console.dir(userId);
		if (userId) 
			return userId;
		else
			Winston.error('createAccount',{user_id: userId, err: 'Failed to create user'});
	},
	userSetUsername: function(username) {
		username = username.toLowerCase();

		var user = Meteor.user();

		check(username,NonEmptyString);
		
		// only set username if user doesn't have one.
		if(!user.username && username.length >= 3 && checkUsername(username))
			Accounts.setUsername(user._id, username);
	},
	userSetEmail: function(email) {
		var user = Meteor.user();
		var userId = user._id;
		var facebook = user.services.facebook;
		check(email,NonEmptyString);

		if(isValidEmailAddress(email) && !facebook)
			Meteor.users.update({_id: userId},{$set: {'emails.0.address': email.toLowerCase()}});
		else
			Winston.error('userSetEmail',{user_id: userId, err: 'Invalid email address or facebook', email: email, facebook: facebook});
	}
});

checkUsername = function(username) {
	var reserved = ['admin', 'administrator', 'blog',	'dev',	'ftp',	'mail',	'pop',	'pop3',	'imap',	'smtp',	'stage',	'stats',	'status',	'www',	'beta',	'about',	'access',	'account',	'accounts',	'add',	'address',	'adm',	'admin',	'administration',	'adult',	'advertising',	'affiliate',	'affiliates',	'ajax',	'analytics',	'android',	'anon',	'anonymous',	'api',	'app',	'apps',	'archive',	'atom',	'auth',	'authentication',	'avatar',	'backup',	'banner',	'banners',	'bin',	'billing',	'blog',	'blogs',	'board',	'bot',	'bots',	'business',	'chat',	'cache',	'cadastro',	'calendar',	'campaign',	'careers',	'cgi',	'client',	'cliente',	'code', 'copyright',	'comercial',	'compare',	'config',	'connect',	'contact',	'contest',	'create',	'code',	'compras',	'css',	'dashboard',	'data',	'db',	'design',	'delete',	'demo',	'design',	'designer',	'dev',	'devel',	'dir',	'directory',	'doc',	'docs',	'domain',	'download',	'downloads',	'edit',	'editor',	'email',	'ecommerce',	'forum',	'forums',	'faq',	'favorite',	'feed',	'feedback',	'flog',	'follow',	'file',	'files',	'free',	'ftp',	'gadget',	'gadgets',	'games',	'guest',	'group',	'groups',	'help',	'home',	'homepage',	'host',	'hosting',	'hostname',	'html',	'http',	'httpd',	'https',	'hpg',	'info',	'information',	'image',	'img',	'images',	'imap',	'index',	'invite',	'intranet',	'indice',	'ipad',	'iphone',	'irc',	'java',	'javascript',	'job',	'jobs',	'js',	'knowledgebase',	'log',	'login',	'logs',	'logout',	'list',	'lists',	'mail',	'mail1',	'mail2',	'mail3',	'mail4',	'mail5',	'mailer',	'mailing',	'mx',	'manager',	'marketing',	'master',	'me',	'media',	'message',	'microblog',	'microblogs',	'mine',	'mp3',	'msg',	'msn',	'mysql',	'messenger',	'mob',	'mobile',	'movie',	'movies',	'music',	'musicas',	'my',	'name',	'named',	'net',	'network',	'new',	'news',	'newsletter',	'nick',	'nickname',	'notes',	'noticias',	'ns',	'ns1',	'ns2',	'ns3',	'ns4',	'ns5',	'ns6',	'ns7',	'ns8',	'ns9',	'ns10',	'old',	'online',	'operator',	'order',	'orders',	'page',	'pager',	'pages',	'panel',	'password',	'perl',	'pic',	'pics',	'photo',	'photos',	'photoalbum',	'php',	'plugin',	'plugins',	'pop',	'pop3',	'post',	'postmaster',	'postfix',	'posts',	'profile',	'project',	'projects',	'promo',	'pub',	'public',	'python',	'random',	'register',	'registration',	'root',	'ruby',	'rss',	'sale',	'sales',	'sample',	'samples',	'script',	'scripts',	'secure',	'send',	'service',	'shop',	'sql',	'signup',	'signin',	'search',	'security',	'settings',	'setting',	'setup',	'site',	'sites',	'sitemap',	'smtp',	'soporte',	'ssh',	'stage',	'staging',	'start',	'subscribe',	'subdomain',	'suporte',	'support',	'stat',	'static',	'stats',	'status',	'store',	'stores',	'system',	'tablet',	'tablets',	'tech',	'telnet',	'test',	'test1',	'test2',	'test3',	'teste',	'tests', 'terms',	'theme',	'themes',	'tmp',	'todo',	'task',	'tasks',	'tools',	'tv',	'talk',	'update',	'upload',	'url',	'user',	'username',	'usuario',	'usage',	'vendas',	'video',	'videos',	'visitor',	'win',	'ww',	'www',	'www1',	'www2',	'www3',	'www4',	'www5',	'www6',	'www7',	'wwww',	'wws',	'wwws',	'web',	'webmail',	'website',	'websites',	'webmaster',	'workshop',	'xxx',	'xpg']

	if(_.indexOf(reserved, username) !== -1)
		return false;
	else
		return true;

}