Meteor.startup(function () {
  smtp = {
  	username: Meteor.settings.Email.username,   // eg: server@gentlenode.com
    password: Meteor.settings.Email.password,   // eg: 3eeP1gtizk5eziohfervU
    server:   Meteor.settings.Email.server,  // eg: mail.gandi.net
    port: 587
  }

  process.env.MAIL_URL = 'smtp://' + encodeURIComponent(smtp.username) + ':' + encodeURIComponent(smtp.password) + '@' + encodeURIComponent(smtp.server) + ':' + smtp.port;


  // By default, the email is sent from no-reply@meteor.com. If you wish to receive email from users asking for help with their account, be sure to set this to an email address that you can receive email at.
  Accounts.emailTemplates.from = Meteor.settings.public.SiteName + ' <' + Meteor.settings.Email.username + '>';

  // The public name of your application. Defaults to the DNS name of the application (eg: awesome.meteor.com).
  Accounts.emailTemplates.siteName = Meteor.settings.public.SiteName;

  // A Function that takes a user object and returns a String for the subject line of the email.
  Accounts.emailTemplates.verifyEmail.subject = function(user) {
    return 'Please Confirm Your Email Address';
  };

  // A Function that takes a user object and a url, and returns the body text for the email.
  // Note: if you need to return HTML instead, use Accounts.emailTemplates.verifyEmail.html
  Accounts.emailTemplates.verifyEmail.html = function(user, url) {
    var html = 'click on the following link to verify your email address: ' + url;
    // console.log(html);
    Meteor.call('sendEmail', 'confirm', user.emails[0].address, {justReturnHtml: true, url: url}, function(error, result){
      if(error)
        Winston.error('onCreateUser confirm email',{user_id: user._id, email: user.emails[0].address, url: url, err: error});
      else
        html = result;
    });
    // console.log(html);
    return html;
  };
});