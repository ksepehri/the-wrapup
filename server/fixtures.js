if (Types.find().count() === 0) { 
    Types.insert(
    {
        id: 'movie', 
        name: 'Movie'
    });
    Types.insert(
    {
        id: 'tv', 
        name: 'TV Show'
    });
    Types.insert(
    {
        id: 'music', 
        name: 'Album'
    });
}

if (Images.find().count() === 0) { 
    Images.insert(
    {
        type: 'movie', 
        name: 'Avengers - Age of Ultron',
        url: '/images/movie/ultron.jpg',
        'background-position-y': '-250px'
    });
    Images.insert(
    {
        type: 'tv', 
        name: 'Game of Thrones',
        url: '/images/tv/gameofthrones.jpg',
        'background-position-y': '-184px'
    });
    Images.insert(
    {
        type: 'music', 
        name: 'Adele - 25',
        url: '/images/music/adele.jpg',
        'background-position-y': '0px'
    });
    Images.insert(
    {
        type: 'img', 
        name: 'banner',
        url: '/images/banner.png',
        'background-position-y': '0px'
    });
}

if (Pantone.find().count() === 0) { 
    Pantone.insert(
    {
        year: 1996, 
        hex: '#7bcc70'
    });
    Pantone.insert(
    {
        year: 1997, 
        hex: '#7bcc70'
    });
    Pantone.insert(
    {
        year: 1998, 
        hex: '#7bcc70'
    });
    Pantone.insert(
    {
        year: 1999, 
        hex: '#7bcc70'
    });
    Pantone.insert(
    {
        year: 2000, 
        hex: '#99c0dd'
    });
    Pantone.insert(
    {
        year: 2001, 
        hex: '#d83d84'
    });
    Pantone.insert(
    {
        year: 2002, 
        hex: '#de1f44'
    });
    Pantone.insert(
    {
        year: 2003, 
        hex: '#81d2d2'
    });
    Pantone.insert(
    {
        year: 2004, 
        hex: '#f66a55'
    });
    Pantone.insert(
    {
        year: 2005, 
        hex: '#1bbcb6'
    });
    Pantone.insert(
    {
        year: 2006, 
        hex: '#e4d6c7'
    });
    Pantone.insert(
    {
        year: 2007, 
        hex: '#b91d43'
    });
    Pantone.insert(
    {
        year: 2008, 
        hex: '#4f71b0'
    });
    Pantone.insert(
    {
        year: 2009, 
        hex: '#f5d16e'
    });
    Pantone.insert(
    {
        year: 2010, 
        hex: '#4ec4bc'
    });
    Pantone.insert(
    {
        year: 2011, 
        hex: '#ed6588'
    });
    Pantone.insert(
    {
        year: 2012, 
        hex: '#f55035'
    });
    Pantone.insert(
    {
        year: 2013, 
        hex: '#1aa890'
    });
    Pantone.insert(
    {
        year: 2014, 
        hex: '#ab86b7'
    });
    Pantone.insert(
    {
        year: 2015, 
        hex: '#995253'
    });
    Pantone.insert(
    {
        year: 2016, 
        hex: '#f8caca'
    });
}