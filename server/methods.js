replaceSpace = function(str){
  return str.replace(/ /g,"+");
};

Meteor.methods({
  searchTVShow: function (show) {
    this.unblock();

    var result = Meteor.http.call("GET", "https://api.themoviedb.org/3/search/tv?api_key=" + process.env.MOVIEDB_KEY + "&query=" + replaceSpace(show));
    //console.dir(result.content);

    return JSON.parse(result.content);
  },
  searchTVSeason: function (show) {
    this.unblock();
    
    var result = Meteor.http.call("GET", "https://api.themoviedb.org/3/tv/" + show + "?api_key=" + process.env.MOVIEDB_KEY);
    //console.dir(result.content);

    var filteredSeasons = JSON.parse(result.content).seasons.filter(function(item){ 
                                    return item.season_number > 0; //exclude season 0 cause that's usually extra features or whatever
                                  });
    return filteredSeasons;
  },
  searchMovie: function (movie) {
    this.unblock();
    //can pass in year= as well to filter
    var url = "https://api.themoviedb.org/3/search/movie?&api_key=" + process.env.MOVIEDB_KEY + "&query=" + replaceSpace(movie);
    //console.dir(url);
    var result = Meteor.http.call("GET", url);
    //console.dir(result.content);

    return JSON.parse(result.content);
  },
  searchMusic: function (artist) {
    this.unblock();
    //use lastfm for artist lookup because their search is good, it doesn't return https images as of 12/8/15 :(
    var result = Meteor.http.call("GET", "https://ws.audioscrobbler.com/2.0/?method=artist.search&artist=" + encodeURI(replaceSpace(artist)) + "&api_key=" + process.env.LASTFM_KEY + "&limit=3&format=json"); //"https://itunes.apple.com/search?term=" + artist.replace(" ","+") + "&entity=musicArtist&limit=3");
    //result = Meteor.http.call("GET", "https://musicbrainz.org/ws/2/artist?query=" + artist.replace(" ","+") + "&fmt=json&limit=3");
    return JSON.parse(result.content);
  },
  searchMusicAlbum: function (artist) {
    this.unblock();

    //itunes release dates are no good but their collection and images are good.
    var url = "https://itunes.apple.com/search?term=" + encodeURI(replaceSpace(artist)) + "&entity=album";

    console.dir(url);
    var result = Meteor.http.call("GET", url);

    var albums = JSON.parse(result.content).results;

    //assume the first artist id is the person you want lol
    var artistId = albums[0].artistId;

    //filter based on that artist and sort by album name for sanity
    var filteredAlbums = albums.filter(function(item){ 
                                    return item.artistId === artistId && item.trackCount > 4; //no EPs!
                                  }).sort(function(a, b){
                                              if(a.collectionName < b.collectionName) return -1;
                                              if(a.collectionName > b.collectionName) return 1;
                                              return 0;
                                          });
    
    filteredAlbums.forEach(function (album){
      album.releaseDate = moment(album.releaseDate).format('MMMM Do YYYY');
    });
    return filteredAlbums;
  },
  getItunesAlbum: function (collectionId) {
    var url = "https://itunes.apple.com/lookup?id=" + collectionId + "&entity=song"
    
    console.dir(url);
    var result = Meteor.http.call("GET", url);

    var albumTracks = JSON.parse(result.content).results.filter(function(item){
      return item.kind === 'song';
    });

    return albumTracks;
  },
  getItunesBuyUrl: function(name, type) {
    //itunes release dates are no good but their collection and images are good.
    // console.dir('hi');
    var theType = 'movie&entity=movie';
    if(type === 'tv')
      theType = 'tvShow&entity=tvSeason';
    var url = "https://itunes.apple.com/search?term=" + encodeURI(replaceSpace(name)) + "&media=" + encodeURI(theType) + "&limit=1";

    console.dir(url);
    
    var itunes = Meteor.http.call("GET", url);

    var media = JSON.parse(itunes.content).results[0];
    
    var buyUrl = null;
    
    if(media) {
      if(media.trackViewUrl)
        buyUrl = media.trackViewUrl;
      else
        buyUrl = media.collectionViewUrl;
    }
    console.dir(buyUrl);
    
    return buyUrl;
  },
  getSpotifyAlbum: function (artist,album) {
    this.unblock();

    var iframe  = '';
    var albumFull = '';
    var url = "https://api.spotify.com/v1/search?q=" + encodeURI(replaceSpace(artist)) + "&type=artist&limit=1"; //encode for non ascii characters
    
    console.dir(url);
    
    var result = Meteor.http.call("GET", url);
    
    //console.dir(JSON.parse(result.content)); 

    var spotifyArtistId = JSON.parse(result.content).artists.items[0].id;

    console.dir(spotifyArtistId); 
       
    url = "https://api.spotify.com/v1/artists/" + spotifyArtistId + "/albums?limit=50"; 
    
    console.dir(url);
    
    result = Meteor.http.call("GET", url);

    //filter based on that artist and try to match the album name lol...
    var albumSummary = JSON.parse(result.content).items.filter(function(item){
                                    //console.dir('item: ' + convertAlbumName(item.name) + ', ' + convertAlbumName(album));
                                    return convertAlbumName(item.name) === convertAlbumName(album); //no EPs!
                                  });

    if(albumSummary[0]){
    console.dir(albumSummary[0].href);

    //get full album details
    result = Meteor.http.call("GET", albumSummary[0].href);
    var albumFull = JSON.parse(result.content);

    if (albumFull)
      iframe = '<iframe src="https://embed.spotify.com/?uri=' + albumFull.uri + '" frameborder="0" allowtransparency="true"></iframe>'
  }
    return {iframe: iframe,album: albumFull};
  },
  getSpotifyUser: function () {
    var redirect_uri = 'https://localhost:3000/callback'; // Your redirect uri
    /**
     * Generates a random string containing numbers and letters
     * @param  {number} length The length of the string
     * @return {string} The generated string
     */
    var generateRandomString = function(length) {
      var text = '';
      var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

      for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    };

    var stateKey = 'spotify_auth_state';

    // your application requests authorization
    var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      headers: {
        'Authorization': 'Basic ' + (new Buffer(process.env.SPOTIFY_CLIENTID + ':' + process.env.SPOTIFY_SECRET).toString('base64'))
      },
      form: {
        grant_type: 'client_credentials'
      },
      json: true
    };
  },
  getTopType: function(year){

    var tops = {
      movie: getTopType('movie',year),
      music: getTopType('music',year), //you can get track names via https://api.spotify.com/v1/tracks/4eE6vZ2vOrceLq4xgz3VmG
      tv: getTopType('tv',year)
    };

    // console.dir(tops.tv);

    return tops;
  },
  getSingleTop: function(type,year,id){
    return getTopType(type,year,id);
  }
});


//methods called by meteor methods
getTopType = function(type,year,id){
  //console.dir(type);
    var group_by = { type: '$type', name: '$name' };
    var match = {};

    if(id) { //an individual wrap has been passed in, find the aggregate of it
      var wrap = Wraps.findOne({_id : id});
      var type = wrap.type;
      _.extend(match, {name: wrap.name});   
      if(type === 'tv')
        _.extend(match, {season_number: wrap.season_number});
      if(type === 'music')
        _.extend(match, {album: wrap.album, collectionId: wrap.collectionId});
      if(type === 'movie' || type === 'tv')
        _.extend(match, {id: wrap.id});
    }

    _.extend(match, {type: type});

    if(type === 'tv')
        _.extend(group_by, {season_number: '$season_number'});
    if(type === 'music')
      _.extend(group_by, {album: '$album', collectionId: '$collectionId'});
    if(type === 'movie' || type === 'tv')
      _.extend(group_by, {id: '$id'});

    if(year !== '*')
      _.extend(match, {year: year});   
    
    pipeline = [
                  {$match : match
                  },
                  {
                    $group : {
                       _id: group_by,
                       count: { $sum: 1 }
                    }
                  },
                  {
                    $sort: {count: -1}
                  },
                  {$limit: 5}
               ];

    return Wraps.aggregate(pipeline);
};

convertAlbumName = function(albumName) {
  //remove all non alphanumeric characters, remove all spaces, and convert to lower case
  return albumName.replace(/\W/g, '').replace(' ','').toLowerCase();
};