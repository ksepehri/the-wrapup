// Winston.silly("Silly!");
// Winston.input("Very verbose");
// Winston.verbose("Just a little wordy");
// Winston.prompt("Not as many words");
// Winston.debug("Good for a coder");
// Winston.info("The standard logging level in winston");
// Winston.data("More important than info")
// Winston.help("Something needs help");
// Winston.warn("Something might be broken");
// Winston.error("Something is broken!!");

Winston = Meteor.npmRequire("winston");
// MongoDB = Npm.require('winston-mongodb').MongoDB;

var Papertrail = Meteor.npmRequire('winston-papertrail').Papertrail;
Winston.add(Papertrail, {
  host: Meteor.settings.Papertrail.host,
  port: Meteor.settings.Papertrail.port, //this will be change from the papertrail account to account
  logFormat: function(level, message) {
      return '[' + level + '] [' + Meteor.settings.public.SiteName + '] ' + message;
  },
  inlineMeta: true
});