// var juice = Meteor.npmRequire("juice");

// var url = absoluteUrl + 'invite/' + inviteId;
// var html = Spacebars.toHTML({ url: url }, Assets.getText('invite.html'));
var css = Assets.getText('style.css');

var absoluteUrl = Meteor.absoluteUrl().replace(/\/$/, "");
// var absoluteUrl = "https://subslice.com/".replace(/\/$/, "");

var siteName = Meteor.settings.public.SiteName;

SSR.compileTemplate('layout', Assets.getText('layout.html'));
SSR.compileTemplate('signature', Assets.getText('signature.html'));
// SSR.compileTemplate('services', Assets.getText('services.html'));

Template.layout.helpers({
	// Blaze does not allow to render templates with DOCTYPE in it.
	// This is a trick to made it possible
	getDocType: function() {
		return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
	},
	siteName: function() {
		return siteName;
	}
});

Template.signature.helpers({
	siteName: function() {
		return siteName;
	}
});

// Template.services.helpers({
// 	url: function() {
// 		return absoluteUrl;
// 	}
// });

var getSubject = function(template, data) {
	var serviceName = (data.service) ? data.service.name : null;
	var subject = {
		confirm: 'Please confirm your email address',
		welcome: 'Welcome to ' + siteName + '!',
		newFollower: 'You have a new follower!'
	};
	return subject[template];
};

Meteor.methods({
  /*
  * Get subscription login/password if the user has access
  */
  'sendEmail': function(template, toEmail, data) {
  		// console.log(template, toEmail, data, this);
  		
  		// TODO: figure out how to wait for logged in before sending email
  		if(!_.contains(['welcome', 'teaser', 'confirm', 'friendRequest'], template))
  			defaultChecks(this);

  		// console.log('template');
		check(template, NonEmptyString);

		// console.log('toEmail');
		check(toEmail, NonEmptyString);

  		if(typeof Email !== 'undefined') {
	  		SSR.compileTemplate(template, Assets.getText(template + ".html"));
	  		// console.log(SSR.render("signature",{siteName: siteName}));
	  		
	  		// add defaults to data
	  		data = _.extend(data, {
	  			siteName: siteName,
	  			absoluteUrl: absoluteUrl,
	  			// signature: SSR.render("signature",{siteName: siteName}) + '<br>HEY'
			});
			
			var html = SSR.render("layout", {
					    css: css,
					    url: absoluteUrl,
					    template: template, // "emailText",
					    data: data
					  });
			// Winston.info(html);
			// console.log(juice("<style>div{color:red;}</style><div/>"));
			// console.log(juice("<html><head><title></title><style>div{color:red;}</style></head><body><div /></body></html>"));
			// console.log(juice(html));
			if(data.justReturnHtml) {
				return juice(html);
			}
			else {
				Email.send({
			      to: toEmail,
			      from: Accounts.emailTemplates.from,
			      subject: getSubject(template, data),
			      html: juice(html)
			    });
			}

		    return true;
		}
		else {
			Winston.error('sendEmail',{user_id: this.userId, template: template, toEmail: toEmail, data: data});
			return false;
		}
	}
});