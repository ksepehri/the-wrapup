// note: houston doesn't show private key of logged in admin
Meteor.publish('user', function() { 
    return Meteor.users.find({_id: this.userId}, {limit: 1, fields: {'emails.address': 1, 'services.facebook.email': 1, 'services.twitter.screenName': 1}});
});

Meteor.publish('wraps', function(limit) {
    if(typeof limit == 'undefined')
        limit = 5;
    var username = null;
    var userId =  this.userId;
    if(userId)
        username = Meteor.users.findOne({_id: userId}).username;
	var clientIp = this.connection.clientAddress;

    Winston.info('user connected', {username: username, userId: userId, ip: clientIp});
    var latestWrapOptions = _.clone(wrapOptions);
    latestWrapOptions = _.extend(latestWrapOptions, {
        sort: {submitted: -1}, 
        limit: limit
    });
	return Wraps.find({}, latestWrapOptions);
});

Meteor.publish('wrap', function(params) {
    return Wraps.find({
                        author: params.author,
                        year: params.year,
                        type: params.type
                });;
});

Meteor.publish('fullWrap', function(wrapId) {
	var wrap = Wraps.find({_id: wrapId});
	return wrap;
});

Meteor.publish('topWrapById', function(wrapId) {
    // console.dir(wrapId);
    var wrap = Wraps.find({id: wrapId}, wrapOptions);
    return wrap;
});

Meteor.publish('topWrapByCollectionId', function(collectionId) {
    // console.dir(collectionId);
    var wrap = Wraps.find({collectionId: collectionId}, wrapOptions);
    return wrap;
});

Meteor.publish('types', function() { 
	return Types.find();
});

Meteor.publish('media', function() { 
    return Media.find();
});

Meteor.publish('images', function() { 
    return Images.find();
});

Meteor.publish('pantone', function() { 
    return Pantone.find();
});

Meteor.publish('hearts', function() { 
	return Hearts.find();
});
// Meteor.publish('friends', function() { 
// 	return Friends.find({userId: this.userId});
// });
Meteor.publish('friends', function(userId) {
	if(userId) {
		var options = {fields: {userId: 1, friendId: 1}};
		var theFriends = friends(userId);
		var users = Meteor.users.find({_id:{ $in: _.pluck(theFriends.fetch(),'friendId')}}, userOptions);
		return [theFriends,users];
	}
	else
		this.ready();
});

Meteor.publish('users', function(username) { 
    var users = Meteor.users.find({username: username}, userOptions);
    // console.dir(users.fetch());
	return users;
});

Meteor.publish('userWraps', function(username) {
    var wraps = Wraps.find({author: username}, wrapOptions);
    // console.dir(wraps.fetch());
    return wraps; 
});

Meteor.publish('userWrapsByYear', function(username,year) {
    var wraps = Wraps.find({author: username, year: year}, wrapOptions);
    // console.dir(wraps.fetch());
    return wraps; 
});

Meteor.publishComposite('friendWraps', {
	find: function() {
        // find current friends of this user
        var curFriends = friends(this.userId);
        // console.log('hi',this.userId,curFriends.fetch());
        return curFriends;
    },
    children: [
        {
            find: function(friend) {
                // Find user account of friend.
                var wraps = Wraps.find({userId: friend.friendId}, wrapOptions);
                // console.log('hi',wraps.count());
                return wraps;
            }
        },
        {
            find: function(friend) {
                // Find user account of friend.
                var wraps = Wraps.find({userId: friend.userId}, wrapOptions);
                return wraps;
            }
        }
    ]
});

var userOptions = {fields: {username: 1, services: 1}};
var wrapOptions = {fields: {
								type: 1, year: 1, image: 1, name: 1, 
								season_number: 1, album: 1, author: 1, 
				   				userId: 1, itunes: 1, spotify: 1,
				   				comment: 1, submitted: 1, id: 1, 
                                collectionId: 1, buyUrl: 1
							}
				   };

var friends = function(userId) { return Friends.find({$or: [{userId: userId}, {friendId: userId}]});};				   
