Template.heartItem.helpers({ 
	heart: function() {
		//console.log(this._id);
		hearts = Hearts.find({wrapId: this._id});
		//console.log(hearts);
		return hearts;
	},
	hearted: function() {
		if(Meteor.user()){
		hearts = Hearts.find({wrapId: Template.instance().data._id, userId: Meteor.user()._id});
		//console.log(hearts);
		return (hearts.count() > 0) ? true:false;
		}
		else return false;
	},
	noLove: function(count) {
		return (count === 0) ? true:false;
	},
	oneLove: function(count) {
		return (count === 1) ? true:false;
	},
	heartAuthors: function() {
		var authorArray = _.pluck(this.fetch(),'author');
		var authors = _.first(authorArray, 2).join(', '); //comma separates first 2 items
		var authorLength = authorArray.length;
		
		if (authorLength > 2){
			var diff = authorLength - 2;
			var end = "";
			if (diff == 1)
				end = "other person";
			else
				end = "more people";
			authors += " and " + diff + " " + end;
		}

		return authors; 
	}
});

Template.heartItem.events({
    'click #heart': function () {
    	//console.log(Template.instance().data._id);
    	$(':focus').blur();
    	if(Meteor.userId()) {
			var heart = {
				wrapId: Template.instance().data._id
			}
			Meteor.call('heartInsert', heart, function(error, result) { 
				// display the error to the user and abort
				if (error)
					return alert(error.reason);

				// show this result but route anyway
				if (result.heartExists)
					return alert('Aw man you already have a heart on this');
		    });
	    }
	    else
	    	$('#signupModal').modal();
    }
});