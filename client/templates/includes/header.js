Template.header.helpers({
	yearlies: function () {
		var wraps = Wraps.find({
						userId: Meteor.user()._id
					},{sort: {year: 1}}).fetch();

		var groupedYears = _.groupBy(_.pluck(wraps, 'year'));

		function Yearlies(year, count) {
			this.year = year;
			this.count = count;
		}

		yearlies = []

		_.each(_.values(groupedYears), function(years) {
		  yearlies.push({year: years[0], count: years.length});
		});
		return yearlies;
	},
	path: function() {
		return '../'+Meteor.user().username+'/'+this.year;
	}
});



var displayName = function(user) {
  if (!user)
    return '';
  if (user.username)
    return user.username;
  if (user.profile && user.profile.name)
    return user.profile.name;
  if (user.emails && user.emails[0] && user.emails[0].address)
    return user.emails[0].address;
  return '';
};

//override default logged in displayName to always be username
Template._loginButtonsLoggedInDropdown.helpers({
  displayName: function() {
  	return displayName(Meteor.user());
  }
});