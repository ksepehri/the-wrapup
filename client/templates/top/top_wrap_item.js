Template.topWrapItem.created = function(){
	var self = this;
	var size = this.data.size;
    self.isLarge = new ReactiveVar(false);
    if(size)
    	self.isLarge.set(true);
};


Template.topWrapItem.events({
	'mouseover .darken': function(event) {
		var id = "#" + this._id;
		$(id).css("display","block");
		while( $(id).height() > $(event.target.parentElement).height() ) {
        	$(id).css('font-size', (parseInt($(id).css('font-size')) - 1) + "px" );
    	}
	},
	'mouseout .darken': function() {
		var id = "#" + this._id;
		$(id).css("display","none");
	}
});

Template.topWrapItem.helpers({
	commentSnippet: function() {
		return snippet(this.comment,50);
	},
	lengthMoreThan: function(data,length){
		return data.length > length;
	},
	compareType: function(type) {
		//console.log(this.name + ' ' + this.type);
		return this.type === type;
	},
	isLarge: function() {
		return Template.instance().isLarge.get();
	},
	clip: function(comments) {
		// console.log('hi');
		return (Template.instance().isLarge.get()) ? comments : _.first(comments,3);
	}
});

var snippet = function(string,max) {
	var snip = null;
	if(string.trimRight().length > max)
		snip = string.substring(0,max+1).trimRight()+'...';
	else
		snip = string;
	return snip;
};