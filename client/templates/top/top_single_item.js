Template.topSingleItem.created = function(){
	var template = this;
	var id = template.data.id;
    template.topItem = new ReactiveArray();
    Meteor.call('getSingleTop','','*',id, function (error, result) {
        if (error)
            console.log(error);
        else 
            topTypeInfo(result,template.topItem);
    });
};

Template.topSingleItem.helpers({
	getAggregate: function() {
		return Template.instance().topItem.list()[0];
	}
});