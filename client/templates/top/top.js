Template.top.created = function() {
	var template = this;
	template.top = new ReactiveVar("");
	template.music = new ReactiveArray();
	template.movie = new ReactiveArray();
	template.tv = new ReactiveArray();
	template.loaded = new ReactiveVar(false);

	Meteor.call('getTopType', '*', function(error, result) { 
			//console.log('hi');
			// display the error to the user and abort
			if (error)
				return alert(error);
			template.top.set(result);
			topTypeInfo(result.music,template.music);
			topTypeInfo(result.movie,template.movie);
			topTypeInfo(result.tv,template.tv);
			template.loaded.set(true);
	    });
}

Template.top.rendered = function(){
	
	$('.header').each(function(index){
		var header = $(this);
		while( header.height() > header.parent().height() ) {
        	header.css('font-size', (parseInt(header.css('font-size')) - 1) + "px" );
    	}
	});
};

Template.top.helpers({
	year: function() {
		return getYears();
	},
	music: function() {
		return Template.instance().music.list();
	},
	movie: function() {
		return Template.instance().movie.list();
	},
	tv: function() {
		return Template.instance().tv.list();
	},
	loaded: function() {
		return Template.instance().loaded.get();
	}
});



Template.top.events({
	'change #year': function(event,template) {
		value = event.target.value;
		template.music.clear();
		template.movie.clear();
		template.tv.clear();
		template.loaded.set(false);
		Meteor.call('getTopType', value, function(error, result) { 
			// display the error to the user and abort
			if (error)
				return alert(error);
			template.top.set(result);
			topTypeInfo(result.music,template.music);
			topTypeInfo(result.movie,template.movie);
			topTypeInfo(result.tv,template.tv);
			template.loaded.set(true);
	    });
	}
});