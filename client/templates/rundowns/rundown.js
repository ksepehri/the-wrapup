var allUsers;

Template.rundown.rendered = function() {
	allUsers = _.pluck(Friends.find().fetch(),'friendId');
	allUsers.push(Meteor.userId());
}

Template.rundown.helpers({ 
	albumRundown: function() {
		// console.log('hi');
		return getType('music');
	},
	movieRundown: function() {
		return getType('movie');
	},
	tvRundown: function() {
		return getType('tv');
	},
	comboFavorites: function() {
		var allUsers = _.pluck(Friends.find().fetch(),'friendId');
		allUsers.push(Meteor.userId());
		musicWraps = Wraps.find({type: 'music', userId: { $in: allUsers}},{spotify: 1});

		if(!musicWraps.count()) return;

		tracks = [];


		musicWraps.forEach(function(item) {
			if(item.spotify)
				tracks = tracks.concat(item.spotify.favoriteTracks)
		});

		//console.log(tracks);
		return tracks;
	}
});

var getType = function(type) {
	var allUsers = _.pluck(Friends.find().fetch(),'friendId');
	allUsers.push(Meteor.userId());

	return Wraps.find({type: type, userId: { $in: allUsers}});
};

//functions

