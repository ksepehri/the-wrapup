// Template.rundownItem.rendered = function() {
// 	var id = "#" + this.data._id;
// 	while( $(id).height() > 200 ) {
//         	$(id).css('font-size', (parseInt($(id).css('font-size')) - 1) + "px" );
//     	}
// }

Template.rundownItem.helpers({
	getAuthor: function () {
		return this.author;
	},
	getYear: function () {
		return this.year;
	},
	showHeader: function () {
		return Template.instance().data.showHeader;
	}
});

Template.rundownItem.events({
	'mouseover .darken': function(event) {
		var id = "#" + this._id;
		var typeId = "#type" + this._id;

		$(typeId).css("display","none");
		$(id).css("display","block");
		while( $(id).height() > $(event.target.parentElement).height() ) {
        	$(id).css('font-size', (parseInt($(id).css('font-size')) - 1) + "px" );
    	}
	},
	'mouseout .darken': function() {
		var id = "#" + this._id;
		var typeId = "#type" + this._id;
		
		$(typeId).css("display","block");
		$(id).css("display","none");
	}
});