Template.wrap_item.helpers({
	ownPost: function() {
		return this.userId === Meteor.userId(); 
	},
	author: function () {
		return Template.instance().data.author;
	}
	// randomColor: function() {
	// 	return randomColor();
	// }
});