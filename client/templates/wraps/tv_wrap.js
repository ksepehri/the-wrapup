Template.tv_wrap.created = function() {
  createSetup("tv");
}


Template.tv_wrap.helpers({
    stuff: function(){
      return Session.get("tvStuff");
    },
    pickedShow: function(){
      return Session.get("wrap");
    }
  });

  Template.tv_wrap.events({
    'click button, keyup #name': function () {
     var show = $("#name").val();
     //console.log(show);
      Meteor.call("searchTVShow", show, function(error, results) {

        shows = sortFilter(results.results);
        
        shows.forEach(function(item){
          item.image = TMDB_IMAGE_92 + item.poster_path;
        });

        //console.log(shows);
        
        Session.set("tvStuff", shows);
    });
    },
    'click #show': function () {
      //console.log(this.id);
      show = this;
      Session.set('show',show);
      Session.set('tvStuff',[show]);

      Meteor.call("searchTVSeason", show.id, function(error, results) {
        seasons = results;
        seasons.forEach(function(item){
          if(item.poster_path !== null)
            item.image = TMDB_IMAGE_92 + item.poster_path;
          else {
              item.image = TMDB_IMAGE_92 + show.poster_path;
              item.poster_path = show.poster_path;
            }
        });
        //console.log(seasons);
        Session.set('seasons',seasons);
        Session.set("displaySeason", true);
      });
    },
    'click #close': function () {
        Session.set("tvStuff", "");
        Session.set("wrapChosen", false);
        Session.set("displaySeason", false);
        Session.set('seasons',"");
        Session.set('show',"");
        Session.set('wrap',"");
    }
  });