Template.tv_season_wrap.created = function() {
	Session.set("seasons", "");
	Session.set("displaySeason", false);
}
  
Template.tv_season_wrap.helpers({
	seasons: function() {
		return Session.get('seasons');
	},
	displaySeason: function() {
		return Session.get("displaySeason");
	}
});

Template.tv_season_wrap.events({
	'click #ddl': function() {
		//e.preventDefault();
		//console.log(this.id);
		Session.set("wrapChosen",true);
		this.show = Session.get("show");
		this.name = this.show.name;
		this.image = TMDB_IMAGE_600 + this.poster_path;
		Session.set("wrap",this);
	}
});