var favoriteTracks = [];
var count = 0;

Template.music_wrap.created = function() {
  createSetup("music");
  Session.set("spotifyPlaylist","");
  favoriteTracks = [];
}

Template.music_wrap.helpers({
    stuff: function(){
      return Session.get("musicStuff");
    },
    pickedAlbum: function(){
      return Session.get("wrap");
    },
    hasFavoriteTracks: function(){
      var wrap = Session.get("wrap");
      // console.log(wrap);
      if(wrap.itunes && wrap.itunes.favoriteTracks && wrap.itunes.favoriteTracks.length === 3)
        return wrap;
      else
        return false;
    }
  });

  Template.music_wrap.events({
    'keyup #name': function () {
     var artist = $("#name").val();
     //console.log(artist);
      Meteor.call("searchMusic", artist, function(error, results) {

        artists = results.results.artistmatches.artist.filter(function(item){
          return item.mbid;
        });
        
        artists.forEach(function(item){
          item.image = item.image[1]["#text"];
        });

        //console.log(artists);
        
        Session.set("musicStuff", artists);
    });
    },
    'click #artist': function (event) {
      // console.log(this);
      Session.set('artist',this);
      Session.set('musicStuff',[this]);

      $(event.target).addClass("scroll-picked");

      Meteor.call("searchMusicAlbum", this.name, function(error, results) {
        albums = results;
        ////console.log(albums);
        albums.forEach(function(item){
          item.image = item.artworkUrl100;
        });
        //console.log(albums);
        Session.set('albums',albums);
        Session.set("displayAlbum", true);
      });
    },
    'click #close': function () {
        Session.set("musicStuff", "");
        Session.set("wrapChosen", false);
        Session.set("displayAlbum", false);
        Session.set('albums',"");
        Session.set('artist',"");
        Session.set('wrap',"");
        Session.set("spotifyPlaylist","");
        favoriteTracks = [];
    },
    'click #track': function (event) {
      if(rgb2hex($(event.target).css("background-color")) === "#add8e6"){
        var index = favoriteTracks.indexOf(this.trackNumber);
        if (index > -1) {
            favoriteTracks.splice(index, 1);
        }
        $(event.target).css("background","");
      } else{
        
        $(event.target).css("background","#add8e6");
        favoriteTracks.push(this.trackNumber);
        if(favoriteTracks.length >= 3){
          // Meteor.call("getSpotifyFavoritesPlaylist", favoriteTracks, function(error, results) {
          //   //console.log(results);
          //   //Session.set("spotifyPlaylist",results);
          // }
          
          var wrap = Session.get("wrap");
          setFavorites(favoriteTracks,wrap);
          Session.set('wrap',wrap);
        }
      }
    },
    'click #repick': function() {
      favoriteTracks = [];
      wrap = Session.get("wrap");
      setFavorites(favoriteTracks,wrap);
      Session.set('wrap',wrap);
    }
  });

var setFavorites = function(favoriteTracks,wrap) {
  var spotifyTracks = [];
  var itunesTracks = [];

  favoriteTracks.forEach(function(track) {
    var tracks = null;
    if(wrap.spotify) {
      tracks = wrap.spotify.tracks;
      spotifyTracks.push(tracks[track-1].id);
    }
    if(wrap.itunes) {
      tracks = wrap.itunes.tracks;
      itunesTracks.push(tracks[track-1].id);
    }
  });

  if(wrap.spotify)
    wrap.spotify.favoriteTracks = spotifyTracks;
  if(wrap.itunes)
    wrap.itunes.favoriteTracks = itunesTracks;
};

Template.tracks.helpers({
  //historical helpers for older spotify faves
    number: function(){
      return (this.track_number) ? this.track_number : this.trackNumber;
    },
    name: function(){
      return (this.name) ? this.name : this.trackName;
    }
  });