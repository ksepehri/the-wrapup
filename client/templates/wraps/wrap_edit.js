Template.wrapEdit.created = function() {
	//console.log(this.data.wrap);
	Session.set("wrap", this.data);
	Session.set("wrapChosen", true);
}

//TODO: make sure spotify favorites is getting cleared out on picking a new album

Template.wrapEdit.events({
	'submit form': function(event) {
		event.preventDefault();
		var wrap = Session.get('wrap');
		wrap._id = this._id;

		wrap.comment = $("#comment").val();

		//need this to update the timestamps that were bad
		//TODO: can probably remove this?
		if(wrap.submitted && wrap.submitted instanceof Date)
			wrap.submitted = wrap.submitted.getTime();

		Meteor.call('wrapUpdate', wrap, function(error, result) { 
			// display the error to the user and abort
			if (error)
				return alert('Something went wrong. Please make sure your wrap is filled out. error: ' + error);

			if (result.dateMismatch)
				return alert('The release year does not match your chosen year');

			if (result.noFaves)
				return alert('Please pick 3 favorite tracks');

			Router.go('wrapPage', {_id: result._id});
	    }); 
	},
	'click .delete': function(event) { 
		event.preventDefault();
		if (confirm("Delete this wrap?")) {
			var wrap = Session.get('wrap');
			var currentWrapId = this._id;
			Meteor.call('wrapDelete', wrap, function(error, result) { 
				// display the error to the user and abort
				if (error)
					return alert('Something went wrong. Please make sure your wrap is filled out. error: ' + error);

				Router.go('main');
		    });   
		} 
	}
});