hideWraps = function() {
	Session.set("wrapChosen", false);
	Session.set("wrap", "");
	Session.set("yearSelected", "");
};

Template.wrapSubmit.created = function() {
	hideWraps();
	this.type = new ReactiveVar("");
}

Template.wrapSubmit.helpers({
	year: function() {
		return getTypeYears(Template.instance().type.get());
	},
	type: function() {
		return Types.find({}, {sort: {name: 1}});
	},
	typeDDL: function() {
		//console.log('type');
		if($("#year").val() !== '')
			return Template.instance().type.get();
		else
			return "";
	},
	isSelected: function(select) {
		return select.value !== "";
	},
	yearSelected: function() {
		if(Session.get("yearSelected") !== "")
			return true;
		else
			return false;
	}
});

Template.wrapSubmit.events({ 
	'change #type': function(event,template) {
		value = event.target.value;
		//console.log(value);
		if(value !== ""){
			$("#year").prop("disabled",false);
			$("#year").val("");
			template.type.set(value);
			Session.set("yearSelected", "");
		}
		else {
			hideWraps();
			$("#year").prop("disabled",true);
		}
	},
	'change #year': function(event,template) {
		Session.set("yearSelected", event.target.value);
	},
	'submit form': function(event) {
	    event.preventDefault();

	    var wrap = Session.get('wrap');
		wrap.year = $("#year").val();
		wrap.type = $("#type").val();
		wrap.comment = $("#comment").val();

		
		Meteor.call('wrapInsert', wrap, function(error, result) { 
			// display the error to the user and abort
			if (error)
				return alert('Something went wrong. Please make sure your wrap is filled out. error: ' + error);

			// show this result but route anyway
			if (result.wrapExists)
				return alert('Aw man you already have a ' + wrap.type + ' wrap for ' + wrap.year);

			if (result.dateMismatch)
				return alert('The release year of the ' + wrap.type + ' does not match your chosen year');

			if (result.noFaves)
				return alert('Please pick 3 favorite tracks');

			Router.go('wrapPage', {_id: result._id});
	    });
	} 
});