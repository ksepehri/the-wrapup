Template.music_album_wrap.created = function() {
	Session.set("albums", "");
	Session.set("tracks", "");
	Session.set("displayAlbum", false);
	Session.set("displayTracks", false);
}
  
Template.music_album_wrap.helpers({
	albums: function() {
		return Session.get('albums');
	},
	displayAlbum: function() {
		return Session.get("displayAlbum");
	},
	displayTracks: function() {
		return Session.get("displayTracks");
	},
	tracks: function() {
		return Session.get('tracks');
	}
});

Template.music_album_wrap.events({
	'click #ddl': function() {
		var album = this;
		//console.log(album.id);
		Session.set("wrapChosen",true);
		album.artist = Session.get("artist");
		album.name = album.artist.name;
		album.album = album.collectionName;
		album.image = album.artworkUrl100.replace("100x100","600x600");
		//album.releaseDate = moment(album.releaseDate).format('MMMM Do YYYY');
		
		//call spotify
        Meteor.call("getSpotifyAlbum", album.artist.name,album.album, function(error, results) {
        	if(results.iframe !== "" && results.album !== ""){
	        	//console.log(results);
	        	album.spotify = {
	        		id: results.album.id,
	        		playlist: results.iframe,
	        		tracks: getTracks(results.album.tracks.items)
	        	};
	        	
	        	Session.set("wrap",album);
        	}
        });

        //get itunes album too
		Meteor.call("getItunesAlbum", album.collectionId, function(error, results){
			// console.log(results);
			album.itunes = {
				tracks: getTracks(results)
			};

			Session.set("wrap",album);
		});

        
	}
});

getTracks = function(data) {
	tracks = [];
	data.forEach(function(item){
		trackNumber = (item.track_number) ? item.track_number: item.trackNumber;
		trackName = (item.name) ? item.name: item.trackName;
		id = (item.id) ? item.id: item.trackId;
		data = item;
		tracks.push({id: id, trackName: trackName, trackNumber: trackNumber, data: data});
	});
	return tracks;
}