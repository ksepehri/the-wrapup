Template.movie_wrap.created = function() {
  createSetup("movie");
}

Template.movie_wrap.helpers({
  stuff: function () {
    return Session.get("movieStuff");
  },
  pickedMovie: function(){
    return Session.get("wrap");
  }
});

Template.movie_wrap.events({
  'click button, keyup #name': function () {
    var movie = $("#name").val();
    //console.log(movie);
    Meteor.call("searchMovie", movie, function(error, results) {
          
      
      movies = sortFilter(results.results);

      movies.forEach(function (item) {
        item.image = TMDB_IMAGE_92 + item.poster_path;
        item.name = item.original_title;
      });
      
      //console.log(movies);
      Session.set("movieStuff", movies);
    });
  },
  'click #movie': function() {
    this.image = TMDB_IMAGE_600 + this.poster_path;
    Session.set("wrap", this);
    Session.set("wrapChosen", true);
  },
    'click #close': function () {
        Session.set("movieStuff", "");
        Session.set("wrapChosen", false);
        Session.set("wrap", "");
    }
});