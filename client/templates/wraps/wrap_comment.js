var maxlength=140;

Template.wrap_comment.rendered = function() {
  var length = this.data.comment && this.data.comment.length || '0';
  $("#commentLength").text(length);
}

Template.wrap_comment.helpers({
    maxlength: function() {
      return maxlength;
    }
});

Template.wrap_comment.events({
  'click button, keyup #comment': function () {
    // console.log('hi');
    $("#commentLength").text($("#comment").val().length);
  }
});