Template.feed.created = function() {
    var template = this;
    template.latestLimit = new ReactiveVar("");
    template.latestLimit.set(5);
};

Template.feed.helpers({
	newest5: function() {
		var latestLimit = Template.instance().latestLimit;
		var options = {sort: {submitted: -1},limit: latestLimit.get()};
		return getWraps(options);
	},
	showMore: function() {
		var wrapCount = getWraps({}).count();
		return (wrapCount > 5) ? true: false;
	}
});

Template.feed.events({
	'click #moreLatest': function(event) {
		$(':focus').blur();
		event.preventDefault();
		var latestLimit = Template.instance().latestLimit;
		latestLimit.set(latestLimit.get()+5);
		// Meteor.subscribe('wraps',latestLimit.get());
	}
});

var following = function(userId) { 
	var friends = Friends.find({userId: userId});
	return _.pluck(friends.fetch(),'friendId')
};	

var getWraps = function(options) {
	var wraps = Wraps.find({$or: [{userId: Meteor.userId()}, {userId: {$in: following(Meteor.userId())}}]},options);
	return wraps;
}