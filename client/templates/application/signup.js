Template.signup.created = function() {
	this.userSuccess = new ReactiveVar("");
}

Template.signup.helpers({
	userSuccess: function () {
		return Template.instance().userSuccess.get();
	},
	service: function() {
		return [{name: 'facebook'},{name: 'twitter'}];
	}
});

Template.signup.events({
	'submit form': function(event) {
	    event.preventDefault();
	    
	    var options = {
	    	username: $(event.target).find("#register-username").val(),
	    	password: $(event.target).find("#register-password").val()
	    }

	    if(!isValidUsername(options.username))
			return alert('Error: Username must be at least 3 characters long');
		if(!isValidPassword(options.password))
			return alert('Error: Password must be at least 6 characters long');
		if(options.password !== $(event.target).find("#register-confirm-password").val())
			return alert('Error: Passwords must match');


		// Accounts.createUser({
  //           username: options.username,
  //           password: options.password
  //       });

	    Meteor.call('createAccount', options, function(error, result) {
	        // display the error to the user and abort
			if (error)
				return alert('Error: ' + error);

			$('#signupModal').modal('hide');
			Meteor.loginWithPassword(options.username, options.password);
			//console.log(result);
			// template.userSuccess.set(result);
			// Router.go('main');
    	});
	}
	// ,
	// 'click button': function(event,template) {
	// 	event.preventDefault();
	// 	$('#signupModal').modal('hide');
	// }
});