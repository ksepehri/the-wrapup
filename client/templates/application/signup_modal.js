Template.signup_modal.rendered = function() {
	//clear cc modal on hide
	$('#signupModal').on('hidden.bs.modal', function(){
		// console.log('hi');
	    // clear the form and errors on dismiss/hide
	    $(this).find('form')[1].reset();
	    $(this).find('.form-group').removeClass("has-error");
	});
};