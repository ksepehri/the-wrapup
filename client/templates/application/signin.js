Template.signin.rendered = function() {
	$('#signupModal').modal('hide');
}

Template.signin.helpers({
	// userSuccess: function () {
	// 	return Template.instance().userSuccess.get();
	// }
});

Template.signin.events({
	'submit form': function(event,template) {
	    event.preventDefault();
	    
	    var options = {
	    	username: $("#signin-username").val(),
	    	password: $("#signin-password").val()
	    }

	    if(!isValidUsername(options.username))
			return alert('Error: Username must be at least 3 characters long');
		if(!isValidPassword(options.password))
			return alert('Error: Password must be at least 6 characters long');

		Meteor.loginWithPassword(options.username, options.password);

		Router.go('main');
	}
});