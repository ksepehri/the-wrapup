Template.main.created = function() {
    var template = this;
    template.latestLimit = new ReactiveVar("");
    template.latestLimit.set(5);
};

Template.main.rendered = function(){
	//calls wodry for main page
	$('.wodry').wodry({
	    animation: 'rotateX',
	    delay: 3000,
	    animationDuration: 1000
	});

	// gets a random image for the background
	// **this is not efficient if the collection is big

	var array = Images.find().fetch();
	var randomIndex = Math.floor( Math.random() * array.length );
	var image = array[randomIndex];
	var linear = 'linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)),';

	if(image){
		var jumbo = $('.main-jumbotron');
		jumbo.css('background',linear+'url("'+image.url+'")');
		var position = image['background-position-y'];
		// if (jumbo.width() > 768)
		// 	position =  image.position.large;
		// else if (jumbo.width() > 400)
		// 	position =  image.position.medium;
		// else if (jumbo.width() > 200)
		// 	position =  image.position.small;

		// position = -0.3*jumbo.width();
			// jumbo.attr('style','@media screen and (min-width: 768px) { background-position-y: '+image.position.large+';}');
		// jumbo.css('background-position-y',position);
	}

};

Template.main.helpers({
	newest5: function() {
		var latestLimit = Template.instance().latestLimit;
		var wraps = Wraps.find({},{sort: {submitted: -1},limit: latestLimit.get()});
		//console.log(wraps);
		return wraps;
	}
});

Template.main.events({
	'click #moreLatest': function(event) {
		$(':focus').blur();
		event.preventDefault();
		// console.log('hi');
		var latestLimit = Template.instance().latestLimit;
		latestLimit.set(latestLimit.get()+5);
		Meteor.subscribe('wraps',latestLimit.get());
	}
});
