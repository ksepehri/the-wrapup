Template.userWraps.helpers({
	areFriends: function() {
		var currentUserId = Meteor.userId();
		var profileId = this._id;
		if(Friends.findOne({userId: currentUserId, friendId: profileId}))
			return true;
		else
			return false;
	},
	years: function () {
		var wraps = Wraps.find({
						userId: this._id
					},{sort: {year: -1}}).fetch();

		return _.union(_.pluck(wraps, 'year'));
	},
	yearWraps: function(year, data) {
		var wraps = Wraps.find({
						userId: data.user._id,
						year: year
					},{sort: {type: 1}});
		return wraps;
	},
	wrapCount: function() {
		return Wraps.find({userId: this._id}).count();
	},
	followerCount: function() {
		return Friends.find({friendId: this._id}).count();
	},
	followingCount: function() {
		return Friends.find({userId: this._id}).count();
	},
	author: function() {
		return Template.instance().data.user.username;
	}
});

Template.userWraps.events({
	'click #add': function(){
    	$(':focus').blur();
    	if(Meteor.userId()) {
    		var friend = {
	    		friendId: this._id
	    	}
	    	Meteor.call('friendInsert', friend, function(error, result) { 
				// display the error to the user and abort
				if (error)
					return alert(error.reason);

				// show this result but route anyway
				if (result.friendExists)
					return alert('Aw man you already have a friend on this');
		    });
    	}
    	else
    		$('#signupModal').modal();
    	
	}
});