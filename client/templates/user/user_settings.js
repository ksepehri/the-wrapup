Template.userSettings.created = function() {
	this.facebook = new ReactiveVar(null);
	var user = Meteor.user();
	if(user.services && user.services.facebook)
		this.facebook.set(user.services.facebook.email);
}

Template.userSettings.helpers({
	fbAccount: function() {
		var facebook = Template.instance().facebook.get();
		// if(facebook) {
		// 	var email = $("#email");
		// 	email.val(facebook);
		// 	email.attr("disabled", "disabled"); 
		// }
		return facebook;
	},
	userEmail: function() {
		var facebook = Template.instance().facebook.get();
		var user = Meteor.user();
		var userEmail = (user.emails) ? user.emails[0].address : null;
		return facebook || userEmail;
	}
});

Template.userSettings.events({
	'submit form': function(event) {
	    event.preventDefault();
	    $("#success").hide();
	    var facebook = Template.instance().facebook.get();
	    if(!facebook) {
	    	var email = $("#email").val();

		    if(isValidEmailAddress(email)) {
		    	Meteor.call('userSetEmail', email, function(error, result) {
		    		
		    		if (error)
						return alert('Something went wrong. Please make sure your email is correct. error: ' + error);
					$("#success").show();
		    	});
		    }
		    else
		    	return alert('Please enter a valid email address');
	    }
	}
});