Template.userMissingFields.events({
	'submit form': function(event) {
		// console.log('hi');
	    event.preventDefault();

	    var username = $("#username").val().replace(/ /g,"");

	    if(username.length > 0) {
	    	Meteor.call('userSetUsername', username, function(error, result) {
	    		
	    		if (error)
					return alert('Username is taken');
	    	});
	    }
	    else
	    	return alert('Please enter a username');
	},
	'keyup #username': function() {
		var username = $("#username").val().replace(/ /g,"");

		if(username.length > 3) {
			Meteor.call("searchUsername", username, function(error, results) {
				if (error)
					return alert('Something went wrong. Can\'t search usernames at this time');

				available(results);
			});
		}
		else
			available(false);
	}
});

Template.userMissingFields.helpers({
	twitterHandle: function() {
		var username = Meteor.user().services.twitter.screenName;
		Meteor.call("searchUsername", username, function(error, results) {
			if (error)
				return alert('Something went wrong. Can\'t search usernames at this time');

			available(results);
		});
		return username;
	}
});

var available = function(bool) {
	if(bool) {
		$(".yes").css("display","inline");
		$(".no").css("display","none");
	}
	else {
		$(".yes").css("display","none");
		$(".no").css("display","inline");
	}
}