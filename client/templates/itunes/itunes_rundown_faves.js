Template.itunesRundownFaves.created = function() {
    var template = this;
    template.currentFavorite = new ReactiveVar("");
    var currentFavorite = null;
    if(template.data.itunes && template.data.itunes.favoriteTracks)
        currentFavorite = 0;
    template.currentFavorite.set(currentFavorite);
};

Template.itunesRundownFaves.helpers({ 
    favoriteTracks: function() {
        // this forces only 3 favorite tracks even if somehow more are saved
        // return _.first(this.itunes.favoriteTracks, 3);
        var currentFavorite = Template.instance().currentFavorite;
        var tracks = null;
        if(this.itunes && this.itunes.favoriteTracks)
            tracks = this.itunes.favoriteTracks[currentFavorite.get()];
        return tracks;
    }
});

Template.itunesRundownFaves.events({
    'click #play': function (event) {
        $(':focus').blur();
        var span = event.currentTarget.childNodes[1];
        var audio = $("#play"+this.id);
        playPause(span,audio);
    },
    'ended .audio': function(event){
        $(':focus').blur();
        var span = event.currentTarget.parentNode.childNodes[1];
        span.className = "glyphicon glyphicon-play";
    },
    'click #forward': function(event) {
        $(':focus').blur();
        var span = $(event.currentTarget.parentNode).find('#play').children()[0];
        var currentFavorite = Template.instance().currentFavorite;
        goToFavorite('forward', currentFavorite, span);
    },
    'click #backward': function(event) {
        $(':focus').blur();
        var span = $(event.currentTarget.parentNode).find('#play').children()[0];
        var currentFavorite = Template.instance().currentFavorite;
        goToFavorite('backward', currentFavorite, span);
    }
});

var goToFavorite = function(direction, currentFavorite, span) {
    span.className = "glyphicon glyphicon-play";
    if(direction === 'forward')
        if(currentFavorite.get() < 2)
            currentFavorite.set(currentFavorite.get() + 1);
        else
            currentFavorite.set(0);
    if(direction === 'backward')
        if(currentFavorite.get() > 0)
            currentFavorite.set(currentFavorite.get() - 1);
        else
            currentFavorite.set(2);


}

var playPause = function(span, audio) {
    var playClass = "glyphicon glyphicon-play";
    var pauseClass = "glyphicon glyphicon-pause";
    if(span.className === playClass){
        //first pause all other tracks
        $("audio").each(function(){
            var audioSpan = this.parentElement.childNodes[1];
            audioSpan.className = playClass;
            // console.log(this);
            this.pause(); // Stop playing
            if(this.currentTime !== 0)
                this.currentTime = 0; // Reset time
        }); 

        //tracks = Session.get("wrap").itunes.tracks;//previewUrl
        span.className = pauseClass;
        audio.trigger("play");
        audio.prop("volume", 0);
        audio.animate({volume: 1}, 1000);
    }
    else{
        span.className = playClass;
        audio.trigger("pause");
    }
}