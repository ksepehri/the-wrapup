Template.itunesFaves.helpers({ 
    favoriteTracks: function() {
        // this forces only 3 favorite tracks even if somehow more are saved
        var tracks = null;
        if(this.itunes && this.itunes.favoriteTracks)
            tracks = _.first(this.itunes.favoriteTracks, 3);
        return tracks;
    }
});

Template.itunesFaves.events({
    'click #play': function (event) {
        var span = event.currentTarget.childNodes[1];
        var audio = $("#play"+this.id);
        playPause(span,audio);
    },
    'ended .audio': function(event){
        var span = event.currentTarget.parentNode.childNodes[1];
        span.className = "glyphicon glyphicon-play";
    }
});

var playPause = function(span, audio) {
    var playClass = "glyphicon glyphicon-play";
    var pauseClass = "glyphicon glyphicon-pause";

    if(span.className === playClass){
        //first pause all other tracks
        $("audio").each(function(){
            var audioSpan = this.parentElement.childNodes[1];
            audioSpan.className = playClass;
            this.pause(); // Stop playing
            if(this.currentTime !== 0)
                this.currentTime = 0; // Reset time
        }); 

        //tracks = Session.get("wrap").itunes.tracks;//previewUrl
        span.className = pauseClass;
        audio.trigger("play");
        audio.prop("volume", 0);
        audio.animate({volume: 1}, 1000);
    }
    else{
        span.className = playClass;
        audio.trigger("pause");
    }
}