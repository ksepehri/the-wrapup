Meteor.startup(function () {

    sAlert.config({
        effect: 'bouncyflip',
        position: 'top-right',
        timeout: 5000,
        html: false,
        onRouteClose: true,
        stack: true,
        // or you can pass an object:
        // stack: {
        //     spacing: 10 // in px
        //     limit: 3 // when fourth alert appears all previous ones are cleared
        // }
        offset: 0, // in px - will be added to first alert (bottom or top - depends of the position in config)
        beep: false
        // examples:
        // beep: '/beep.mp3'  // or you can pass an object:
        // beep: {
        //     info: '/beep-info.mp3',
        //     error: '/beep-error.mp3',
        //     success: '/beep-success.mp3',
        //     warning: '/beep-warning.mp3'
        // }
    });

    // Keep track of how many followers a user has.
    Tracker.autorun(function () {
      // console.log('hi');
      var count = 0;
      var query = Friends.find({friendId: Meteor.userId()});
      var lastFriendId = null;
      var init = true;
      var handle = query.observeChanges({
        added: function (id, friend) {
          // only show notification if the last friend wasn't the same id
          // this keeps multiple notifications from popping up if someone is spamming
          if(!lastFriendId) {
            lastFriendId = friend.userId;
          }
          else
            init = false;

          //check if follower added in last 2 seconds, this is to avoid multiple notifications
          //TODO: figure out how to get observeChanges to only run on adds not initial load
          if(moment(friend.submitted) >= moment().subtract(2,'seconds') && (init || lastFriendId !== friend.userId)) {
            sAlert.info('You have a new follower!');
            lastFriendId = friend.userId;
          }
        },
        removed: function () {
          // don't say anything, that's mean
        }
      });
    });

});

Accounts.ui.config({
  passwordSignupFields: 'USERNAME_ONLY'
});

//function to sort, filter, and splice an array
sortFilter = function (array) {
	return array.filter(function(item){
          return item.poster_path !== null && item.poster_path !== "null";
        }).sort(function(a, b){
                                                      if(a.collectionName < b.collectionName) return -1;
                                                      if(a.collectionName > b.collectionName) return 1;
                                                      return 0;
                                                  }).splice(0,3);
}

var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 

//Function to convert hex format to a rgb color
rgb2hex = function (rgb) {
 rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
 return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hex(x) {
  return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

function hex2rgba(hex,opacity){
    hex = hex.replace('#','');
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);

    result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
    return result;
}

//template create setup
createSetup = function(type) {
  // console.log('hi');
  Session.set(type+"Stuff", "");
    if(Session.get("wrap")._id === undefined)
      Session.set("wrapChosen", false);
};

//get spotify iframe, if it's favorite tracks then handle them separately
spotifyIframe = function(uri){
  iframe_head = '<iframe src="https://embed.spotify.com/?uri=';
  iframe_tail = '" frameborder="0" allowtransparency="true"></iframe>';
  iframe_mid = '';
  if(uri.constructor === Array)
    iframe_mid = 'spotify:trackset:favorites:' + uri.join(',');
  else
    iframe_mid = uri;

  full_iframe = iframe_head + iframe_mid + iframe_tail;

  //console.log(full_iframe);

  return full_iframe;
}; 

getYears = function() {
  var currentYear = new Date().getFullYear();
  years = [];
  startYear = 1996;

  while ( startYear <= currentYear ) {
    years.push(currentYear--);
  } 
  return years;
};

getTypeYears = function(type) {
  var currentYear = new Date().getFullYear();
  years = [];
  startYear = 1996;

  //probably shouldn't call collection here

  var typeYears = Wraps.find({userId: Meteor.user()._id, type: type},{year:1}).fetch();

  while ( startYear <= currentYear) {
    if(_.where(typeYears,{year: currentYear.toString()}).length === 0)
      years.push(currentYear--);
    else
      currentYear--;
  } 
  return years;
};

//doc title

document.title = Meteor.settings.public.SiteName;


//TMDB defaults

TMDB_IMAGE = "https://image.tmdb.org/t/p/";

TMDB_IMAGE_92 = TMDB_IMAGE + "w92";

TMDB_IMAGE_600 = TMDB_IMAGE + "w600";

//global helpers

UI.registerHelper(
  "log", function (object) {
      if(object)
        console.log(object);
      else
        console.log(this);
});

UI.registerHelper(
	"getTypeName", function(type) {
		return Types.findOne({id: type}).name;
});

UI.registerHelper(
  "getTypeIcon", function(type) {
    var icon = null;
    switch (type) {
      case 'tv':
        icon = 'fa-television';
        break;
      case 'movie':
        icon = 'fa-film';
        break;
      case 'music':
        icon = 'fa-music';
        break;
    }
    return icon;
});

UI.registerHelper(
 	"equalObject", function(ob1,ob2) {
    	return ob1 === ob2;
});

UI.registerHelper(
  "header", function() {
    header = this.name;

    if(this.season_number)
      header += ' - Season ' + this.season_number;
    if(this.album)
      header += ' - ' + this.album;
    return header;
});

UI.registerHelper(
  "getSpotifyIframe", function(uri) {
    return spotifyIframe(uri);
});

UI.registerHelper(
  "itunesAffiliate", function () {
      return Meteor.settings.public.itunesAffiliate;
});

UI.registerHelper(
  "itunesTrack", function(trackId,wrap){
    // console.log(trackId,wrap);
    tracks = wrap.itunes.tracks;
    return _.where(tracks,{id: trackId})[0];
});

UI.registerHelper(
  'firstLetter',function(str) {
    return str.substring(0,1).toUpperCase();
});

UI.registerHelper(
  "siteName", function() {
    return Meteor.settings.public.SiteName;
});

UI.registerHelper(
  "pantone", function(year) {
    // console.log('hi');
    var hex = Pantone.findOne({year: parseInt(year)}).hex;
    return 'background:'+hex2rgba(hex,40)+';';
});

UI.registerHelper(
  "wrapChosen", function() {
    return Session.get("wrapChosen");
});

UI.registerHelper('instance', function() {
  // console.log(Template.instance());
  return Template.instance();
});

UI.registerHelper(
  "getWrapId", function(wrap) {
    // console.log('hi');
    return wrap._id;
});

UI.registerHelper(
  "prettyTime", function(time) {
    return moment(time).calendar();
});

//session defaults
Session.setDefault("movieStuff", "");
Session.setDefault("wrapChosen", false);
Session.setDefault("albums", "");
Session.setDefault("displayAlbum", false);
Session.setDefault("musicStuff", "");
Session.setDefault("spotifyPlaylist", "");
Session.setDefault("seasons", "");
Session.setDefault("displaySeason", false);
Session.setDefault("tvStuff", "");
Session.setDefault("showtv", false);
Session.setDefault("showmovie", false);
Session.setDefault("showmusic", false);
Session.setDefault("showsubmit", false);
Session.setDefault("wrap", "");