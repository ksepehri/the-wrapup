isValidEmailAddress = function(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

isValidPassword = function(val) {
	return val.length >= 6 ? true : false; 
};

isValidUsername = function(val) {
	return val.trim().length >= 3 ? true : false; 
};

// check that the userId specified owns the documents
ownsDocument = function(userId, doc) { 
	// console.log('hi');
	return (doc && doc.userId === userId);
};

NonEmptyString = Match.Where(function (x) {
	check(x, String);
	return x.length > 0;
});

// 1. get one of each top
// 2. get all comments for each top and combine
// TODO: see if you can move this into the server?
topTypeInfo = function(data,array){
	// tops.set([]);

	if(!data)
		return null;
	data.forEach(function(top){

		if(top.count > 1) { //ghetto filtering top five until i figure out how to do it in the aggregate. TODO: fix this.
			var item = top._id;
			var wraps = null;
			var mainwrap = null;
			if(item.id) {
				Meteor.subscribe('topWrapById',item.id,function() {
					wraps = Wraps.find({id: item.id});
					mainwrap = wraps.fetch()[0];

					var comments = topComments(wraps);
					array.push({wrap: mainwrap,comments: _.sortBy(comments,'count').reverse()});
				});
			}
			else {
				Meteor.subscribe('topWrapByCollectionId',item.collectionId,function() {
					// console.log('hi');
					wraps = Wraps.find({collectionId: item.collectionId});
					var tempwrap = wraps.fetch()[0];
					if(tempwrap.itunes)
						tempwrap.itunes.favoriteTracks = getTopTracks(wraps);
					mainwrap = tempwrap;

					var comments = topComments(wraps);
					array.push({wrap: mainwrap,comments: _.sortBy(comments,'count').reverse()});
				});	
			}
		}

	});
	// return tops;
};

function Comment(author,year,type,comment,count,color) {
	this.author = author;
	this.year = year;
	this.type = type;
	this.comment = comment;
	this.count = count;
	this.color = color;
};

topComments = function(wraps) {
	var comments = [];

	//combine all comments
	wraps.forEach(function(item){
		if(item.comment !== ''){
			//get heart count for each wrap so we can sort comments by popularity
			var heartCount = Hearts.find({wrapId: item._id}).count();

			var color = randomColor({
								   luminosity: 'light',
								   hue: 'blue'
								}); //TODO: make colors complementary
			comments.push(new Comment(item.author,item.year,item.type,item.comment,heartCount,color));
		}
	});

	return comments;
}

getTopTracks = function(wraps) {
	var topTracks = [];
	var allTracks = [];
	wraps.forEach(function(item){
		if(item.itunes){
			allTracks = allTracks.concat(item.itunes.favoriteTracks);
		}
	});
	//console.log(allTracks);

	var hist = {};
	_.countBy(allTracks,function (a) { if (a in hist) hist[a] ++; else hist[a] = 1; } );

	var t = [];
	for(key in hist){
		t.push({track: parseInt(key), count: hist[key]});
	}

	//sorts t by count, then picks the last 3 because sort is asc, reverses to get 123 instead of 321, 
	//then plucks just the tracks from obj
	topTracks = _.pluck(_.last(_.sortBy(t,'count'),3).reverse(),'track'); 

	return topTracks.length === 0 ? null : topTracks; 
	//return allTracks;
};