Meteor.startup(function() {
    if (Meteor.isClient) {
        var siteName = Meteor.settings.public.SiteName;
        var absoluteUrl = Meteor.absoluteUrl().replace(/\/$/, "");

        return SEO.config({
            title: siteName + ' - Share Your Favorites',
            meta: {
                'description': 'Share your favorite movie/music/tv show',
                'keywords': 'yearlies, the yearlies, share, movie, video, music, tv, tv shows, stream, streaming, itunes'
            },
            og: {
                'image': absoluteUrl + '/images/logo.png' 
           }
        });
    }
});