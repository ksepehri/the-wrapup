(function(){
	Friends = new Mongo.Collection('friends');

	Friends.allow({
		insert: function(userId, doc) {
		    // only allow if you are logged in
			return !! userId; 
		}, 
		remove: function(userId, friend) { return ownsDocument(userId, friend); }
	});

	Meteor.methods({
		friendInsert: function(friendAttributes) {
			NonEmptyString = Match.Where(function (x) {
			  check(x, String);
			  return x.length > 0;
			});
			// console.dir(friendAttributes);
			check(friendAttributes, {
				friendId: NonEmptyString
			});

			var user = Meteor.user();
			var friend = _.extend(friendAttributes, {
				userId: user._id,
				submitted: new Date()
			});

			var friendExists = Friends.findOne({
				userId: friendAttributes.userId, 
				friendId: friendAttributes.friendId
			});

			//if the friend exists that means this user has already been friended by the current user
			//remove it 
			if (friendExists) {
				Friends.remove(friendExists._id);
				return false;
			}

			var friendId = Friends.insert(friend);
			return {
				_id: friendId
			}; 
		}
	});
})();