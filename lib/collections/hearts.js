(function(){
	Hearts = new Mongo.Collection('hearts');

	Hearts.allow({
		insert: function(userId, doc) {
		    // only allow if you are logged in
			return !! userId; 
		}, 
		remove: function(userId, heart) { return ownsDocument(userId, heart); }
	});

	Meteor.methods({
		heartInsert: function(heartAttributes) {
			NonEmptyString = Match.Where(function (x) {
			  check(x, String);
			  return x.length > 0;
			});
			//console.dir(heartAttributes);
			check(heartAttributes, {
				wrapId: NonEmptyString //id of album/movie/season
			});

			var user = Meteor.user();
			var heart = _.extend(heartAttributes, {
				userId: user._id, 
				author: user.username, 
				submitted: new Date()
			});

			var heartExists = Hearts.findOne({
				userId: heartAttributes.userId, 
				wrapId: heartAttributes.wrapId
			});

			//if the heart exists that means this wrap has already been hearted by the user
			//remove it 
			if (heartExists) {
				Hearts.remove(heartExists._id);
				return false;
			}

			var heartId = Hearts.insert(heart);
			return {
				_id: heartId
			}; 
		}
	});
})();