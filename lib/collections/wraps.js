(function(){
	Wraps = new Mongo.Collection('wraps');

	Wraps.allow({
		insert: function(userId, doc) {
		    // only allow posting if you are logged in
			return !! userId; 
		},
		update: function(userId, doc) { return ownsDocument(userId, doc); }, 
		remove: function(userId, doc) { return ownsDocument(userId, doc); }
	});

	// Wraps.deny({
	// 	update: function(userId, wrap, fieldNames) {
	// 	    // may only edit the following two fields:
	// 		return (_.without(fieldNames, 'url', 'title').length > 0); 
	// 	}
	// });

	var NonEmptyString = Match.Where(function (x) {
			  check(x, String);
			  return x.length > 0;
			});

	Meteor.methods({
		wrapInsert: function(wrapAttributes) {
			
			//console.dir(wrapAttributes);
			check(this.userId, String); 
			check(wrapAttributes.name,NonEmptyString);
			check(wrapAttributes.year,NonEmptyString);
			check(wrapAttributes.type,NonEmptyString);

			var user = Meteor.user();

			var wrap = _.extend(wrapAttributes, {
				userId: user._id, 
				author: user.username, 
				submitted: Date.parse(new Date())
			});

			if(wrap.itunes && (!wrap.itunes.favoriteTracks || wrap.itunes.favoriteTracks.length === 0))
				return {noFaves: true};

			// maxlength of comment
			wrap.comment = wrap.comment.substr(0,140);

			var duplicateWrap = Wraps.findOne({
				userId: wrapAttributes.userId, 
				type: wrapAttributes.type,
				year: wrapAttributes.year
			}); 
			if (duplicateWrap) {
				return {
					wrapExists: true,
					_id: duplicateWrap._id
				} 
			}

			//check to make sure the year is right
			// releaseDate = (typeof wrap.releaseDate !== undefined) ? moment(wrap.releaseDate,'MMMM Do YYYY') : moment(wrap.release_date);
			// //console.dir(releaseDate);

			// if (wrap.year !== releaseDate.format('YYYY'))
			// 	return {dateMismatch: true};
			if(wrap.type !== 'music') {
				var season = (wrap.season_number) ? ' - season ' + wrap.season_number: '';
				var name = wrap.name + season;

				wrap.buyUrl = Meteor.call('getItunesBuyUrl',name,wrap.type);
			}

			var wrapId = Wraps.insert(wrap);
			return {
				_id: wrapId
			};
		},
		wrapUpdate: function(wrapAttributes) {
			// console.dir(wrapAttributes);
			if(!ownsDocument(Meteor.user()._id,wrapAttributes))
				throw new Meteor.Error(500,'Can only update your own wraps','only own wraps');
			wrap = wrapAttributes;
			currentWrapId = wrap._id;

			var wrapExists = Wraps.findOne({
				_id: currentWrapId
			});

			if(wrapExists){
				//check to make sure the year is right
				// releaseDate = (typeof wrap.releaseDate !== undefined) ? moment(wrap.releaseDate,'MMMM Do YYYY') : moment(wrap.release_date);
				// //console.dir(releaseDate);

				// if (wrapExists.year !== releaseDate.format('YYYY'))
				// 	return {dateMismatch: true};

				// maxlength of comment
				wrap.comment = wrap.comment.substr(0,140);

				if(wrap.itunes && (!wrap.itunes.favoriteTracks || wrap.itunes.favoriteTracks.length === 0))
					return {noFaves: true};

				if(wrap.type !== 'music') {
					var season = (wrap.season_number) ? ' - season ' + wrap.season_number: '';
					var name = wrap.name + season;

					wrap.buyUrl = Meteor.call('getItunesBuyUrl',name,wrap.type);
				}

				
				delete wrap._id;
				Wraps.update(currentWrapId, {$set: wrap});
				return {
					_id: currentWrapId
				};
			}
			else
				return {error: 'could not find wrap to update'};
		},
		wrapDelete: function(wrapAttributes) {
			// console.dir(wrapAttributes);
			if(!ownsDocument(Meteor.user()._id,wrapAttributes))
				throw new Meteor.Error(500,'Can only delete your own wraps','only own wraps');
			wrap = wrapAttributes;
			currentWrapId = wrap._id;

			var wrapExists = Wraps.findOne({
				_id: currentWrapId
			});

			if(wrapExists){
				//check to make sure the year is right
				// releaseDate = (typeof wrap.releaseDate !== undefined) ? moment(wrap.releaseDate,'MMMM Do YYYY') : moment(wrap.release_date);
				// //console.dir(releaseDate);

				// if (wrapExists.year !== releaseDate.format('YYYY'))
				// 	return {dateMismatch: true};

				
				Wraps.remove(currentWrapId);
				return {
					_id: currentWrapId
				};
			}
			else
				return {error: 'could not find wrap to delete'};
		}
	});

})();