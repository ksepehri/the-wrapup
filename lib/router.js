//from http://www.manuel-schoebel.com/blog/meteorjs-iron-router-filters-before-and-after-hooks
var IR_Filters = {
    // All standard subscriptions you need before anything works
    // the .wait() makes sure that it continues only if the subscription
    // is ready and the data available
    // Use: global
    baseSubscriptions: function() {
        this.subscribe('userData').wait();
    },
    //show main page if logged in user is trying to access
    requireLoggedOut: function() { 
        if (Meteor.loggingIn() || Meteor.user()) {
            Router.go('/');
        } else {
            this.next(); 
        }
    },
    //show access denied if anonymous user is trying to access
    //logged in user areas
    requireLogin: function() { 
        if (! Meteor.user()) {
            if (Meteor.loggingIn()) { 
                this.render(this.loadingTemplate);
            } else { 
                this.render('accessDenied');
            }
        } else {
            this.next(); 
        }
    },
    // check if the user owns this subscription in order to display
    checkAccess: function() {
        // console.dir(this);
        if(!ownsDocument(Meteor.userId(),this.data()))
            this.render('accessDenied');
        else
            this.next();
    },
    // check to see if user has a username, if not force userMissingFields template
    checkProfile: function() {
        var user = Meteor.user()
        //console.dir(user);
        if(user) {
            if(!user.username) {
                this.render('userMissingFields');
            }
            else
                this.next();
        }
        else
            this.next();
    },
    // show login if a guest wants to access private areas
    // Use: {only: [privateAreas] }
    isLoggedIn: function(pause) {
        if (!(Meteor.loggingIn() || Meteor.user())) {
          Notify.setError(__('Please login.')); // some custom packages
          this.render('login');
          pause();
        }
    },
    // make sure to scroll to the top of the page on a new route
    // Use: global
    scrollUp: function() {
        $('body,html').scrollTop(0);
    },
    // add extra buttons to account dropdown
    loginExtraButtons: function() {
        var user = Meteor.user();
        //check if user is signed in and that desired HTML element does not already exists
        if (user && ($('#profileButton').length===0) || $('#settingsButton').length===0) {
            var newHTML = "<a href='" + Router.path('profile') + "' class='btn btn-default btn-block' id='profileButton'>Profile</a>";
            newHTML += "<a href='" + Router.path('userSettings') + "' class='btn btn-default btn-block' id='settingsButton'>Settings</a>";
            //Add desired HTML above the change password button
            $('#login-buttons-open-change-password').before(newHTML);
        }
        this.next();
    },
    // if this route depends on data, show the NProgess loading indicator
    // http://ricostacruz.com/nprogress/
    // Use: global
    startNProgress: function() {
        if (_.isFunction(this.data)) {
          NProgress.start();
        }
    },
    // tell google analytics that a page was viewed
    // e.g. https://github.com/datariot/meteor-ganalytics
    // Use: global
    pageview: function() {
        GAnalytics.pageview(this.path);
    },
    // only show route if you are an admin
    // using https://github.com/alanning/meteor-roles
    // Use: {only: [adminAreas]}
    isAdmin: function(pause) {
        if (!Roles.userIsInRole(Meteor.userId(), ['admin'])) {
        // console.dir('hi');
        // console.dir(Houston._user_is_admin());
        // if (!Houston._admins.findOne({user_id: Meteor.userId()})) {
          this.render('login');
          pause();
        }
    },
    // animate old content out using
    // http://daneden.github.io/animate.css/
    // Use: global
    animateContentOut: function() {
        $('#content').removeClass("animated fadeIn fadeInRight");
        $('footer').addClass("hide");
    }
};

Router.configure({
layoutTemplate: 'layout',
loadingTemplate: 'loading',
notFoundTemplate: 'notFound'
,waitOn: function() { return [Meteor.subscribe('wraps'), Meteor.subscribe('types'),
                              Meteor.subscribe('hearts'), Meteor.subscribe('friends',Meteor.userId()),
                              Meteor.subscribe('friendWraps'), Meteor.subscribe('images'),
                              Meteor.subscribe('pantone'), Meteor.subscribe('media'), 
                              Meteor.subscribe('user')]; }
});

var siteName = Meteor.settings.public.SiteName;

Router.route('/', {name: 'main'});

Router.route('/list', {name: 'wrapsList'});

Router.route('/submit', {name: 'wrapSubmit'});

// Router.route('/rundown', {name: 'rundown'});

Router.route('/about', {
    name: 'about',
    onAfterAction: function() {
      // The SEO object is only available on the client.
      // Return if you define your routes on the server, too.
      if (!Meteor.isClient) {
        return;
      }
      var title = siteName + ' - About';
      var description = 'About The Yearlies';
      SEO.set({
        title: title,
        meta: {
          'description': description 
        },
        og: {
          'title': title,
          'description': description
        }
      });
    }
});

Router.route('/top', {name: 'top'});

Router.route('/signup', {
    name: 'signup',
    onAfterAction: function() {
      // The SEO object is only available on the client.
      // Return if you define your routes on the server, too.
      if (!Meteor.isClient) {
        return;
      }
      var title = siteName + ' - Sign Up';
      var description = 'Sign up to The Yearlies';
      SEO.set({
        title: title,
        meta: {
          'description': description 
        },
        og: {
          'title': title,
          'description': description
        }
      });
    }
});

Router.route('/signin', {name: 'signin'});

Router.route('/terms', {name: 'terms'});

Router.route('/privacy', {name: 'privacy'});

Router.route('/copyright', {name: 'copyright'});

Router.route('/feed', {name: 'feed'});

Router.route('/settings', {name: 'userSettings'});

Router.route('/profile', {
    template: 'userWraps',
    data: function() { return {user:Meteor.user()}; }
});

Router.route('/top/:_id', {
	name: 'topSingleItem',
	data: function() { return {id: this.params._id}; }
});

Router.route('/wrap/:_id', {
	name: 'wrapPage',
	data: function() { return Wraps.findOne(this.params._id); }
});

Router.route('/wrap/:_id/edit', {
    name: 'wrapEdit',
    waitOn: function() { return [Meteor.subscribe('fullWrap',this.params._id)];},
    data: function() { 
        // console.log('hi');
        return Wraps.findOne(this.params._id); 
    }
});

Router.route('/:author/:year/:type/:_id', {
    name: 'wrapPageUser',
    controller: 'wrapController'
});

Router.route('/:author/:year/:type', {
    name: 'wrapPageUserNoId',
    controller: 'wrapController'
});

wrapController = RouteController.extend({
    template: 'wrapPage',
    waitOn: function() { return [Meteor.subscribe('wrap',this.params)];},
    data: function() {
        // var user = Meteor.users.findOne({username:this.params.author}); 
        var data = Wraps.findOne(this.params._id);
        var that = this;
        // console.log('hi');
        // id wasn't valid for some reason
        if(!data)
            data = Wraps.findOne({
                        author: that.params.author,
                        year: that.params.year,
                        type: that.params.type
                });
        return data; 
    }
});

Router.route('/:author/:year', {
	name: 'yearlyWrap',
    waitOn: function() {
        return [Meteor.subscribe('userWrapsByYear',this.params.author, this.params.year)];
    },
	data: function() {
		return {
                wraps: Wraps.find({
						author: this.params.author,
						year: this.params.year
				        }),
                year: this.params.year}
                ;
		 }
});

Router.route('/:author',{
	name: 'userWraps',
    waitOn: function() { var user = Meteor.users.findOne({username:this.params.author});
                         var userId = (user)? user._id: null;
                         return [Meteor.subscribe('users',this.params.author), 
                                 Meteor.subscribe('userWraps',this.params.author),
                                 Meteor.subscribe('friends',userId)];},
	data: function() { return {user:Meteor.users.findOne({username:this.params.author})}; }
});

Router.route('/:author/friends',{
    name: 'userFriends',
    waitOn: function() { return [
                            Meteor.subscribe('users',this.params.author)
                            // ,
                            // Meteor.subscribe('friends',Meteor.userId())
                            ];
                        },
    data: function() { return {
                        user: Meteor.users.findOne({username:this.params.author})
                        //friends: Friends.find({userId: this.params.author}),
                        };
                     }
});

// Router.route('/profile',{
//     name: 'userProfile'
// });
if (Meteor.isClient) {

Router.onBeforeAction(IR_Filters.loginExtraButtons); //Injects HTML every time before the page loads

Router.onBeforeAction(IR_Filters.requireLogin, {only: ['wrapSubmit', 'wrapEdit', 'userMissingFields', 'feed', 'userSettings']});

Router.onBeforeAction(IR_Filters.checkAccess, {only: ['wrapEdit']});

Router.onBeforeAction(IR_Filters.requireLoggedOut, {only: ['signup']});

Router.onBeforeAction(IR_Filters.checkProfile);
Router.onAfterAction(IR_Filters.scrollUp, {except: ['main']});

}

