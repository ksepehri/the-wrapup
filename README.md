# The Wrapup

##About

A place to list and share your favorite album, movie, and tv show for each year.

##How to get it

<ol>
  <li>Clone this repository
  <li>Install <a href="https://www.meteor.com/install">Meteor</a>
  <li>Get API keys for <a href="http://www.last.fm/api/account/create">LastFM</a> and <a href="https://www.themoviedb.org/documentation/api">TheMovieDB</a> and add them to <a href="https://github.com/ksepehri/the-wrapup/blob/master/server/keys.js">keys.js</a>
  <li>Run meteor
</ol>
```bash
    your-machine:the-wrapup you$ meteor
    [[[[[ ~/the-wrapup ]]]]]                      

    => Started proxy.                             
    => Started MongoDB.                           
    => Started your app.                          
    
    => App running at: http://localhost:3000/
```
